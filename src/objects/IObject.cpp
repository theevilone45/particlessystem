//
// Created by theev on 22.08.2021.
//

#include "objects/IObject.h"

ps::IObject::IObject() : _id(global_object_id++) {
}

unsigned int ps::IObject::id() const {
    return _id;
}

ps::ObjectType ps::IObject::type() const {
    return ps::ObjectType::BASE;
}
