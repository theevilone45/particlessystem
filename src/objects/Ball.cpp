//
// Created by theev on 22.08.2021.
//

#include "objects/Ball.h"

ps::Ball::Ball(sf::RenderWindow *render_window, const sf::Vector2<float> &position, const float radius,
               const sf::Color &color)
        : _radius(radius),
          _velocity(0.0f, 0.0f),
          _acceleration(0.0f, 0.0f),
          _friction(0.985f),
          _render_window(render_window) {
    _shape.setRadius(_radius);
    _shape.setOrigin(_radius, _radius);
    _shape.setFillColor(color);
    _shape.setPosition(position);
}

ps::Ball::~Ball() {
    std::cout << "Killed" << std::endl;
}

void ps::Ball::render() {
    _render_window->draw(_shape);
}

void ps::Ball::update(const float& elapsed_time) {
//    acceleration() = -1.f * velocity() * friction();
    velocity() *= friction();
    velocity() += acceleration() * elapsed_time;
    _shape.move(velocity() * elapsed_time );
    updateWindowBorderCollisions();
}

void ps::Ball::setColor(unsigned char r, unsigned char b, unsigned char g) {
    _shape.setFillColor(sf::Color(r, g, b));
}

const ps::velocity_t &ps::Ball::velocity() const {
    return _velocity;
}

ps::velocity_t &ps::Ball::velocity() {
    return _velocity;
}

const ps::acceleration_t &ps::Ball::acceleration() const {
    return _acceleration;
}

ps::acceleration_t &ps::Ball::acceleration() {
    return _acceleration;
}

bool ps::Ball::isInCollisionWithLeftBorder() {
    return _shape.getPosition().x < 0;
}

bool ps::Ball::isInCollisionWithRightBorder() {
    return _shape.getPosition().x > static_cast<float>(_render_window->getSize().x);
}

bool ps::Ball::isInCollisionWithTopBorder() {
    return _shape.getPosition().y <= _radius;
}

bool ps::Ball::isInCollisionWithBottomBorder() {
    return _shape.getPosition().y >= static_cast<float>(_render_window->getSize().y) - _radius;
}

void ps::Ball::updateWindowBorderCollisions() {
    if (isInCollisionWithLeftBorder()) {
        _shape.setPosition(static_cast<float>(_render_window->getSize().x), _shape.getPosition().y);
//        velocity().x *= -1;
    }
    if (isInCollisionWithRightBorder()) {
        _shape.setPosition(
                0,
                _shape.getPosition().y
        );
//        velocity().x *= -1;
    }
    if (isInCollisionWithTopBorder()) {
        _shape.setPosition(_shape.getPosition().x, _radius);
        velocity().y *= -1;
    }
    if (isInCollisionWithBottomBorder()) {
        _shape.setPosition(
                _shape.getPosition().x,
                static_cast<float>(_render_window->getSize().y) - _radius - 1
        );
        velocity().y *= -1;
    }
}

void ps::Ball::setColor(const sf::Color &color) {
    setColor(color.r, color.b, color.g);
}

const sf::Vector2<float> &ps::Ball::position() const {
    return _shape.getPosition();
}

void ps::Ball::position(const sf::Vector2<float> position) {
    _shape.setPosition(position);
}

float ps::Ball::radius() const {
    return _shape.getRadius();
}

void ps::Ball::radius(const float radius) {
    _shape.setRadius(radius);
}

ps::ObjectType ps::Ball::type() const {
    return ps::ObjectType::BALL;
}

void ps::Ball::move(const sf::Vector2<float> &displacement) {
    _shape.move(displacement);
}

const float &ps::Ball::friction() const {
    return _friction;
}

float &ps::Ball::friction() {
    return _friction;
}
