//
// Created by theevilone45 on 15.09.2021.
//

#include "Timer.h"

Timer::Timer() {
    start = std::chrono::high_resolution_clock::now();
}

float Timer::endTimer() {
    end = std::chrono::high_resolution_clock::now();
    duration = end - start;
    return 1.0f / duration.count() / 100;
}