//
// Created by theev on 23.08.2021.
//

#include "App.h"

namespace ps {

    App::App() : _running(false), _fps(0) {
        _context_settings.antialiasingLevel = _settings.antialiasing_level();
        _render_window.create(
                sf::VideoMode(_settings.resolution().first, _settings.resolution().second),
                _settings.title(),
                sf::Style::Default,
                _context_settings
        );
        _simulation_info_text.setRenderWindow(&_render_window);
        _gravity = _settings.gravity();
    }

    void App::addObject(const std::shared_ptr<IObject> &object) {
        _objects.push_back(object);
    }

    void App::handleEvents() {
        sf::Event event{};
        while (_render_window.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed:
                    _running = false;
                    break;
                case sf::Event::KeyReleased:
                    handleKeyReleasedEvent(event);
                    break;
                default:
                    break;
            }
        }
    }

    void App::render() {
        _render_window.clear();
        _simulation_info_text.render();
        for (const auto &obj: _objects) {
            obj->render();
        }

        _render_window.display();
    }

    void App::update(const float &elapsed_time) {
        simulation_info_t sim_info = {_fps, _objects.size(), (_gravity != 0)};
        _simulation_info_text.update(sim_info);
        for (const auto &obj: _objects) {
            obj->update(elapsed_time);
            if (obj->type() == ObjectType::BALL) {
                std::dynamic_pointer_cast<Ball>(obj)->acceleration().y = _gravity;
            }
        }
        updateBallStaticCollisions();
        updateBallDynamicCollisions();
    }

    void App::run() {
        _running = true;
        sf::Clock clock;
        sf::Time time_since_last_update = sf::Time::Zero;
        const sf::Time time_per_frame = _settings.time_per_frame();

        while (_running) {
            Timer timer;
            handleEvents();
            time_since_last_update += clock.restart();

            while (time_since_last_update > time_per_frame) {
                time_since_last_update -= time_per_frame;
                handleEvents();
                update(time_per_frame.asSeconds());
            }

            render();
            _fps = timer.endTimer();
        }
        _render_window.close();
    }

    App::~App() = default;

    void App::updateBallStaticCollisions() {
        float distance_between_objects_squared;

        auto does_balls_overlap = [&distance_between_objects_squared](std::shared_ptr<Ball> &ball1,
                                                                      std::shared_ptr<Ball> &ball2) {
            float sum_of_radii_squared;
            distance_between_objects_squared =
                    (ball1->position().x - ball2->position().x) * (ball1->position().x - ball2->position().x) +
                    (ball1->position().y - ball2->position().y) * (ball1->position().y - ball2->position().y);
            sum_of_radii_squared = (ball1->radius() + ball2->radius()) * (ball1->radius() + ball2->radius());
            if (distance_between_objects_squared < sum_of_radii_squared)
                return true;
            return false;
        };

        for (auto &obj1: _objects) {
            if (obj1->type() != ObjectType::BALL)
                continue;
            for (auto &obj2: _objects) {
                if (obj1->id() == obj2->id())
                    continue;
                if (obj2->type() != ObjectType::BALL)
                    continue;
                auto ball1 = std::dynamic_pointer_cast<Ball>(obj1);
                auto ball2 = std::dynamic_pointer_cast<Ball>(obj2);

                if (does_balls_overlap(ball1, ball2)) {
                    float distance = sqrtf(distance_between_objects_squared);
                    _collision_balls.emplace_back(distance, ball1, ball2);
                    float overlap = 0.5f * (distance - ball1->radius() - ball2->radius());
                    ball2->move(overlap * (ball1->position() - ball2->position()) / distance);
                    ball1->move(-1 * overlap * (ball1->position() - ball2->position()) / distance);
                }
            }
        }
    }

    void App::handleKeyReleasedEvent(const sf::Event &event) {
        sf::Vector2<float> mouse_position = Converters::getMousePositionF(&_render_window);
        switch (event.key.code) {
            case sf::Keyboard::A:
                addBall(mouse_position, 30, sf::Color(255, 0, 0));
                break;
            case sf::Keyboard::Space:
                addVelocityToBalls();
                break;
            case sf::Keyboard::G:
                switchGravity();
            default:
                break;
        }
    }

    void App::addBall(const sf::Vector2<float> &position, const float radius, const sf::Color &color) {
        addObject(std::make_shared<Ball>(&_render_window, position, radius, color));
    }

    void App::addVelocityToBalls() {
        for (const auto &obj: _objects) {
            if (obj->type() != ObjectType::BALL)
                continue;

            std::dynamic_pointer_cast<Ball>(obj)->velocity() = {Randomizer::nextFloat(-1000, 1000),
                                                                Randomizer::nextFloat(-1000, 1000)};
        }
    }

    void App::switchGravity() {
        _gravity = (_gravity == 0) ? _settings.gravity() : 0;
    }

    void App::minimalTextTest() {
        sf::Font font;
        if (!font.loadFromFile("assets/fonts/Anonymous.ttf")) {
            std::cout << "load font failed" << std::endl;
            return;
        }

        sf::Text text;
        text.setString("kupa");
        text.setFont(font);

//Set character size
        text.setCharacterSize(24);

//Set fill color
        text.setFillColor(sf::Color::White);


        while (_render_window.isOpen()) {
            sf::Event event{};
            while (_render_window.pollEvent(event)) {
                if (event.type == sf::Event::Closed) { _render_window.close(); }
            }

            //Clear the _render_window
            _render_window.clear();
            //Draw the text
            _render_window.draw(text);
            //Display the text to the _render_window
            _render_window.display();
        }
    }

    void App::updateBallDynamicCollisions() {
        auto dot_product = [](const sf::Vector2<float>& vec1, const sf::Vector2<float>& vec2){
            return vec1.x * vec2.x + vec1.y * vec2.y;
        };

        for(auto & pair: _collision_balls)
        {
            float mass1 = pair.obj1->radius()/100;
            float mass2 = pair.obj2->radius()/100;
            
            float mass_factor = (2*mass2)/(mass2+mass1);
            float dot_product_factor = dot_product(
                    pair.obj1->velocity()-pair.obj2->velocity(),
                    pair.obj1->position()-pair.obj2->position()) / (pair.distance*pair.distance);
            sf::Vector2<float> v1 = mass_factor * dot_product_factor * (pair.obj1->position() - pair.obj2->position());

            mass_factor = (2*mass1)/(mass2+mass1);
            dot_product_factor = dot_product(
                    pair.obj2->velocity()-pair.obj1->velocity(),
                    pair.obj2->position()-pair.obj1->position()) / (pair.distance*pair.distance);
            sf::Vector2<float> v2 = mass_factor * dot_product_factor * (pair.obj2->position() - pair.obj1->position());
            std::cout << "v1.x: " << v1.x << std::endl;
            pair.obj1->velocity() -= v1;
            pair.obj2->velocity() -= v2;
        }
        _collision_balls.clear();
    }
}
