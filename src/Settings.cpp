//
// Created by theev on 22.08.2021.
//

#include "Settings.h"

void ps::Settings::loadSettings() {
    readWindowSettingsFromFile();
    readRenderSettingsFromFile();
    readPhysicsSettingsFromFile();
}

const ps::resolution_t &ps::Settings::resolution() const {
    return _resolution;
}

const std::string &ps::Settings::title() const {
    return _title;
}

const unsigned int &ps::Settings::fps() const {
    return _fps;
}

void ps::Settings::readWindowSettingsFromFile() {
    const std::string window_settings_file_name = "config/window.json";
    nlohmann::json window_settings_json = readSettingsFromFile(window_settings_file_name);

    _resolution = std::make_pair(
            window_settings_json["resolution"]["width"],
            window_settings_json["resolution"]["height"]
    );
    _title = window_settings_json["title"];
}

void ps::Settings::readRenderSettingsFromFile() {
    const std::string render_settings_file_name = "config/render.json";
    nlohmann::json render_settings_json = readSettingsFromFile(render_settings_file_name);

    _fps = static_cast<unsigned int>(render_settings_json["FPS"]);
    _time_per_frame = sf::seconds(1.0f/static_cast<float>(_fps));
    _antialiasing_level = static_cast<unsigned int>(render_settings_json["antialiasing_level"]);
}

const unsigned int &ps::Settings::antialiasing_level() const {
    return _antialiasing_level;
}

const sf::Time &ps::Settings::time_per_frame() const {
    return _time_per_frame;
}

ps::Settings::Settings() {
    loadSettings();
}

void ps::Settings::readPhysicsSettingsFromFile() {
    const std::string physics_settings_file_name = "config/physics.json";
    nlohmann::json physics_settings_json = readSettingsFromFile(physics_settings_file_name);

    _gravity = static_cast<float>(physics_settings_json["gravity"]);
}

const float &ps::Settings::gravity() const {
    return _gravity;
}

nlohmann::json ps::Settings::readSettingsFromFile(const std::string &file_path) {
    std::ifstream json_file(file_path);
    if (!json_file)
        throw std::runtime_error(std::string("cannot open file: " + file_path));

    nlohmann::json settings_json = nlohmann::json::parse(json_file);

    json_file.close();
    return settings_json;
}
