#include <cstdlib>
#include "App.h"

int main() {
    ps::App app;
    app.run();
    app.minimalTextTest();
    return EXIT_SUCCESS;
}