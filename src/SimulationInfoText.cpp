//
// Created by theevilone45 on 13.09.2021.
//

#include "SimulationInfoText.h"

SimulationInfoText::SimulationInfoText(): _render_window(nullptr){
    loadFont("assets/fonts/Anonymous.ttf");
    setUpTexts();
}

SimulationInfoText::~SimulationInfoText() {
    _render_window = nullptr;
}

void SimulationInfoText::render() {
    if(_render_window == nullptr)
        return;
    _render_window->draw(_fps_text);
    _render_window->draw(_amount_of_objects_text);
    _render_window->draw(_gravity_state_text);
}


void SimulationInfoText::loadFont(const std::string& path) {
    bool font_loading_result = _font.loadFromFile(path);
    if(!font_loading_result)
        throw std::runtime_error(std::string("Font was not loaded properly"));
}

void SimulationInfoText::update(const simulation_info_t & sim_info) {
    _sim_info = sim_info;
    _fps_text.setString("FPS: " + std::to_string(sim_info.fps));
    _amount_of_objects_text.setString("Amount of objects: " + std::to_string(sim_info.amount_of_objects));
    std::string gravity_state = (sim_info.gravity_state)?"ON":"OFF";
    _gravity_state_text.setString("Gravity: " + gravity_state);
}

void SimulationInfoText::setUpTexts() {
    _fps_text.setFont(_font);
    _fps_text.setCharacterSize(24);
    _fps_text.setFillColor(sf::Color::White);
    _fps_text.setOutlineColor(sf::Color::Black);
    _fps_text.setPosition(10,10);

    _amount_of_objects_text.setFont(_font);
    _amount_of_objects_text.setCharacterSize(24);
    _amount_of_objects_text.setFillColor(sf::Color::White);
    _amount_of_objects_text.setOutlineColor(sf::Color::Black);
    _amount_of_objects_text.setPosition(10,44);

    _gravity_state_text.setFont(_font);
    _gravity_state_text.setCharacterSize(24);
    _gravity_state_text.setFillColor(sf::Color::White);
    _gravity_state_text.setOutlineColor(sf::Color::Black);
    _gravity_state_text.setPosition(10, 78);
}

void SimulationInfoText::setRenderWindow(sf::RenderWindow *render_window) {
    _render_window = render_window;
}

std::ostream& operator<<(std::ostream& os, const simulation_info_t& sim_info)
{
    os << "Simulation State:" << std::endl;
    os << "\tFPS:               " << sim_info.fps << std::endl;
    os << "\tAmount of objects: " << sim_info.amount_of_objects << std::endl;
    os << "\tGravity state:     " << ((sim_info.gravity_state) ? "ON" : "OFF") << std::endl;
    return os;
}
