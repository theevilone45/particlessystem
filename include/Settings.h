//
// Created by theev on 22.08.2021.
//

#ifndef PARTICLESSYSTEM_SETTINGS_H
#define PARTICLESSYSTEM_SETTINGS_H

#include <tuple>
#include <fstream>

#include "../include/json.hpp"

#include "SFML/Graphics.hpp"

namespace ps {
    using resolution_t = std::pair<unsigned int, unsigned int>;
    class Settings {
        resolution_t _resolution;
        std::string _title;
        unsigned int _fps{};
        sf::Time _time_per_frame;
        unsigned int _antialiasing_level{};
        float _gravity;
    public:
        Settings();

        [[nodiscard]] const resolution_t& resolution() const;
        [[nodiscard]] const std::string& title() const;
        [[nodiscard]] const unsigned int& fps() const;
        [[nodiscard]] const unsigned int& antialiasing_level() const;
        [[nodiscard]] const  sf::Time& time_per_frame() const;
        [[nodiscard]] const float& gravity() const;
        void loadSettings();
    private:
        void readWindowSettingsFromFile();
        void readRenderSettingsFromFile();
        void readPhysicsSettingsFromFile();

        static nlohmann::json readSettingsFromFile(const std::string& file_path);
    };
}


#endif //PARTICLESSYSTEM_SETTINGS_H
