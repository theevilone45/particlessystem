//
// Created by theev on 23.08.2021.
//

#ifndef PARTICLESSYSTEM_APP_H
#define PARTICLESSYSTEM_APP_H

#include <SFML/Graphics.hpp>
#include "Settings.h"
#include "objects/Objects.h"
#include <vector>
#include <tuple>
#include <SimulationInfoText.h>
#include <Timer.h>

namespace ps {

    class App {

        // [0] => distance, [1] => first_collision_ball, [2] => second_collision_ball
//        using collision_pair_t = std::tuple<float, std::shared_ptr<Ball>, std::shared_ptr<Ball>>;

        struct collision_pair_t {
            float distance;
            std::shared_ptr<Ball> obj1;
            std::shared_ptr<Ball> obj2;
        };

    private:
        Settings _settings;
        sf::RenderWindow _render_window;
        sf::ContextSettings _context_settings;
        bool _running;
        std::vector<std::shared_ptr<IObject>> _objects;
        std::vector<collision_pair_t> _collision_balls;
        float _gravity;
        SimulationInfoText _simulation_info_text;
        float _fps;

    public:
        App();

        ~App();

        void run();

        void minimalTextTest();

    private:
        void addObject(const std::shared_ptr<IObject> &object);

        void handleEvents();

        void handleKeyReleasedEvent(const sf::Event &event);

        void render();

        void update(const float &elapsed_time);

        void updateBallStaticCollisions();

        void updateBallDynamicCollisions();

        void addBall(const sf::Vector2<float> &position, float radius, const sf::Color &color);

        void addVelocityToBalls();

        void switchGravity();
    };
}


#endif //PARTICLESSYSTEM_APP_H
