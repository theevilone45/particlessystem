//
// Created by theevilone45 on 13.09.2021.
//

#ifndef PARTICLESSYSTEM_SIMULATIONINFOTEXT_H
#define PARTICLESSYSTEM_SIMULATIONINFOTEXT_H

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <iostream>

struct simulation_info_t
{
    float fps;
    size_t amount_of_objects;
    bool gravity_state;
};

std::ostream& operator<<(std::ostream& os, const simulation_info_t& sim_info);

class SimulationInfoText {

public:
    explicit SimulationInfoText();
    ~SimulationInfoText();
    void render();
    void update(const simulation_info_t& sim_info);
    void setRenderWindow(sf::RenderWindow *render_window);

private:
    sf::Text _fps_text;
    sf::Text _amount_of_objects_text;
    sf::Text _gravity_state_text;

    sf::RenderWindow *_render_window{};
    sf::Font _font;

    simulation_info_t _sim_info;

    void loadFont(const std::string& path);
    void setUpTexts();
};


#endif //PARTICLESSYSTEM_SIMULATIONINFOTEXT_H
