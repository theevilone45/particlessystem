//
// Created by theev on 23.08.2021.
//

#ifndef PARTICLESSYSTEM_UTILS_H
#define PARTICLESSYSTEM_UTILS_H

#include <random>
#include <chrono>
#include <iostream>
#include <utility>



namespace ps {

    using namespace std::chrono_literals;

    class Randomizer {
    public:
        static int nextNumber(int min, int max) {
            unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
            static std::default_random_engine generator(seed);
            std::uniform_int_distribution<int> distribution(min, max);
            return distribution(generator);
        }

        static float nextFloat(float min, float max) {
            unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
            static std::default_random_engine generator(seed);
            std::uniform_real_distribution<float> distribution(min, max);
            return distribution(generator);
        }
    };

    struct Converters {
        static sf::Vector2<float> getMousePositionF(sf::RenderWindow *_window) {
            return {
                    static_cast<float>(sf::Mouse::getPosition(*_window).x),
                    static_cast<float>(sf::Mouse::getPosition(*_window).y)
            };
        }
    };

}

#endif //PARTICLESSYSTEM_UTILS_H
