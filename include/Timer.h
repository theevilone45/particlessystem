//
// Created by theevilone45 on 15.09.2021.
//

#ifndef PARTICLESSYSTEM_TIMER_H
#define PARTICLESSYSTEM_TIMER_H

#include <chrono>
#include <iostream>

struct Timer {
    std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
    std::chrono::duration<float> duration;

    Timer() ;
    float endTimer();
};


#endif //PARTICLESSYSTEM_TIMER_H
