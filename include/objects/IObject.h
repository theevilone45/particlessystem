//
// Created by theev on 22.08.2021.
//

#ifndef PARTICLESSYSTEM_IOBJECT_H
#define PARTICLESSYSTEM_IOBJECT_H

#include <memory>
#include <SFML/Graphics.hpp>

namespace ps {
    enum class ObjectType
    {
        BASE,
        BALL
    };

    class Window;
    class World;

    static unsigned int global_object_id = 0;

    class IObject {
    protected:
        unsigned int _id;
        IObject();

    public:
        virtual void render() = 0;
        virtual void update(const float& elapsed_time) = 0;

        [[nodiscard]] unsigned int id() const;
        [[nodiscard]] virtual ObjectType type() const;
    };
}


#endif //PARTICLESSYSTEM_IOBJECT_H
