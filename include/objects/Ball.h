//
// Created by theev on 22.08.2021.
//

#ifndef PARTICLESSYSTEM_BALL_H
#define PARTICLESSYSTEM_BALL_H

#include <random>

#include "objects/IObject.h"

#include <SFML/Graphics.hpp>
#include <iostream>
#include "../utils.h"

namespace ps {

    using velocity_t = sf::Vector2<float>;
    using acceleration_t = sf::Vector2<float>;

    class Ball : public IObject {
    public:
        Ball(sf::RenderWindow *render_window, const sf::Vector2<float> &position, float radius, const sf::Color &color);

        ~Ball();

        void render() override;

        void update(const float& elapsed_time) override;

        [[nodiscard]] ObjectType type() const override;

        void updateWindowBorderCollisions();

        void setColor(unsigned char r, unsigned char g, unsigned char b);

        void setColor(const sf::Color &color);

        void move(const sf::Vector2<float> &displacement);

        inline bool isInCollisionWithLeftBorder();

        inline bool isInCollisionWithRightBorder();

        inline bool isInCollisionWithTopBorder();

        inline bool isInCollisionWithBottomBorder();

        [[nodiscard]] const velocity_t &velocity() const;

        velocity_t &velocity();

        [[nodiscard]] const acceleration_t &acceleration() const;

        acceleration_t &acceleration();

        [[nodiscard]] const float &friction() const;

        float &friction();

        [[nodiscard]] const sf::Vector2<float> &position() const;

        void position(sf::Vector2<float> position);

        [[nodiscard]] float radius() const;

        void radius(float radius);

    private:
        float _radius;
        velocity_t _velocity;
        acceleration_t _acceleration;
        float _friction;
        sf::CircleShape _shape;
        sf::RenderWindow *_render_window;
    };
}

#endif //PARTICLESSYSTEM_BALL_H
