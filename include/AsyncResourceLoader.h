//
// Created by theev on 23.08.2021.
//

#ifndef PARTICLESSYSTEM_ASYNCRESOURCELOADER_H
#define PARTICLESSYSTEM_ASYNCRESOURCELOADER_H

#include <iostream>
#include <future>
#include <utility>
#include <vector>
#include <chrono>
#include <functional>
#include <SFML/Graphics.hpp>

template<typename T, typename F>
class AsyncResourceLoader {
private:
    std::vector<T> _resources;
    std::vector<std::future<void>> _futures;
    std::mutex _resourcesMutex;
    F _loading_function;
public:
    explicit AsyncResourceLoader(F loading_function)
    :_loading_function(loading_function)
    {
    }

    void loadResource(const std::string &path) {
        using namespace std::chrono_literals;
        T resource = _loading_function(path);
        std::lock_guard<std::mutex> lock(_resourcesMutex);
        _resources.push_back(resource);
        std::cout << "Resource " << path << " loaded correctly" << std::endl;
    }

    void loadResources(const std::vector<std::string> &paths) {
        for (const auto &path : paths) {
            _futures.push_back(std::async(std::launch::async,
                                          [this](auto &&PH1) { loadResource(std::forward<decltype(PH1)>(PH1)); },
                                          path));
        }
    }
};

#endif //PARTICLESSYSTEM_ASYNCRESOURCELOADER_H
